import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'region',
        loadChildren: './region/region.module#ElasticsearchRegionModule'
      },
      {
        path: 'country',
        loadChildren: './country/country.module#ElasticsearchCountryModule'
      },
      {
        path: 'location',
        loadChildren: './location/location.module#ElasticsearchLocationModule'
      },
      {
        path: 'department',
        loadChildren: './department/department.module#ElasticsearchDepartmentModule'
      },
      {
        path: 'task',
        loadChildren: './task/task.module#ElasticsearchTaskModule'
      },
      {
        path: 'employee',
        loadChildren: './employee/employee.module#ElasticsearchEmployeeModule'
      },
      {
        path: 'job',
        loadChildren: './job/job.module#ElasticsearchJobModule'
      },
      {
        path: 'job-history',
        loadChildren: './job-history/job-history.module#ElasticsearchJobHistoryModule'
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ElasticsearchEntityModule {}
