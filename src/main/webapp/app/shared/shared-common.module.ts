import { NgModule } from '@angular/core';

import { ElasticsearchSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [ElasticsearchSharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  exports: [ElasticsearchSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class ElasticsearchSharedCommonModule {}
